#!/bin/bash

usage="$(basename "$0") [-h] [-n, -c] -- get a bed file with masked regions from a list of masked fasta compressed files

where:
    -h  show this help text
    -n  set the BASENAME of your fasta file in the following form: BASENAME.chromosome.1.fa.gz
    -c  set the chromosome number of the organism (it should correspond to your file number)"


while getopts ":n:c:h" opt; do
  case "$opt" in
    h) echo "$usage"
       exit
       ;;
    n) basename=$OPTARG
       ;;
    :) printf "Missing argument for -%s\n\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
    c) chromosomes=$OPTARG
       ;;
    :) printf "Missing argument for -%s\n\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
shift $(( OPTIND - 1 ))

if [[ -z $basename || -z $chromosomes ]]; then
    echo -e "Missing mandatory parameters.\n" >&2
    echo "$usage" >&2
    exit 1
fi
	
if [ ! -f ${basename}.chromosome.1.fa.gz ]; then
    echo -e "File for chr01 with the given basename not found.\n" >&2
    echo "$usage"
    exit 1
fi

module load tools/parallel/20120822

dochr() {

    chr=$1
    basename=$2
    mychr=$(printf %02d $chr)

    zcat ${basename}.chromosome.${chr}.fa.gz | unhead | sed -e 's/[GATCN]\{1\}/&\n/gI' | sed -e '/^$/d' | grep -n -i 'N' | cut -f 1 -d: > Ncoords${chr}

    last=$(tail -1 Ncoords${chr})
    prev_coord="-1"
    while IFS='' read -r coord || [[ -n "$coord" ]]; do
        if [ "$prev_coord" == "-1" ]; then
            start=$coord
        elif [ $(($coord-1)) != $prev_coord -o "$coord" == "$last" ]; then
            end=$prev_coord
            echo -e "chr${mychr}\t${start}\t${end}"
            start=$coord
        fi
        prev_coord=$coord
    done < "Ncoords${chr}" > Ncoords${chr}.bed
    rm Ncoords${chr}

}

export -f dochr
parallel --jobs 10 dochr {1} {2} ::: $(seq ${chromosomes}) ::: $basename

cat Ncoords*.bed > masked.bed
rm Ncoords*.bed
